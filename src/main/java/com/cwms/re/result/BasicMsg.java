package com.cwms.re.result;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class BasicMsg {
    String msg;
    /**
     * 200成功
     * 402失败
     */
    int code;
    Object data ;
}
