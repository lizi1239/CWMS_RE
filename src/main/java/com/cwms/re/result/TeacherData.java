package com.cwms.re.result;

import com.cwms.re.pojo.Teacher;
import com.cwms.re.pojo.User;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
 public class TeacherData {
    Teacher teacher;
    User user;
    String code;
    String msg;
}
