package com.cwms.re.result;

import com.cwms.re.pojo.Student;
import com.cwms.re.pojo.User;
import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class StudentData {
    Student student;
    User user;
    String code;
    String msg;
}
