package com.cwms.re.result;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class RegData {
    String account;
    String userName;
    String msg;
    int code;
}
