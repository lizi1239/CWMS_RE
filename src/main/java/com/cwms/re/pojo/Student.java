package com.cwms.re.pojo;

import lombok.Data;
import org.springframework.stereotype.Component;

/**
 * 用途：
 *      与数据库表对应
 * 作者：PSL
 * 日期：2021.06.13
 * 版本：0.0.1
 */
@Data
@Component   //标注，使能注入
public class Student {
    private String stuId;
    private String account;
    private String name;
}
