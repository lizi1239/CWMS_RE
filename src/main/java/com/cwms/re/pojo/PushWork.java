package com.cwms.re.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("push_work")
public class PushWork {

  private String classId;
  private String workId;
  private String path;
  private String workName;



}
