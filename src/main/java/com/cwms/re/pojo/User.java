package com.cwms.re.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 用途：
 *      与数据库表对应
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Data //简化set、get
@AllArgsConstructor
@NoArgsConstructor
@TableName("user")
//@Component   //标注，使能注入
public class User {
    private String account ;
    private String password;
    private String userName;
    private String type;
    private String salt;
}
