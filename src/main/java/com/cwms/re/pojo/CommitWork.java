package com.cwms.re.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("commit_work")
public class CommitWork {

  private String workId;
  private String stuId;
  private String classId;
  private String path;
  private String workName;


}
