package com.cwms.re.pojo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class StuClass {

  private String stuId;
  private String classId;
  private String teacherName;
  private String className;
  private String studentName;


}
