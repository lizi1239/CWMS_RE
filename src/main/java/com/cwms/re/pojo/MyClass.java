package com.cwms.re.pojo;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("class")
public class MyClass {

  private String classId;
  private String teacherId;
  private String className;
  private String teacherName;
}



