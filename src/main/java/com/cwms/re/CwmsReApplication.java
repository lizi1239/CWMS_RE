package com.cwms.re;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//这个包下的所有接口都是 mapper
@MapperScan("com.cwms.re.mapper")
@SpringBootApplication
public class CwmsReApplication {

	public static void main(String[] args) {
		SpringApplication.run(CwmsReApplication.class, args);
	}

}
