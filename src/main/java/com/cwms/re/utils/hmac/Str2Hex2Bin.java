package com.cwms.re.utils.hmac;

import javax.xml.bind.annotation.adapters.HexBinaryAdapter;

public class Str2Hex2Bin {
    /**
     * 字符串转换成为16进制(无需Unicode编码)
     * byte 8 位
     * char 16位
     * int  32位
     * 过程：getBytes      bs[i]&0x0f0>>4
     *         16->8           8->4
     *
     * @param str
     * @return
     */
    public static String str2HexStr(String str) {
        //给定编码规则
        char[] chars = "0123456789ABCDEF".toCharArray();

        StringBuilder sb = new StringBuilder("");
        //16 -> 8
        byte[] bs = str.getBytes();
        int bit;
        for (int i = 0; i < bs.length; i++) {
            //提取byte高4位，获得4位码
            bit = (bs[i] & 0x0f0) >> 4;
            //将这4位码对应的16进制字符添加到缓存
            sb.append(chars[bit]);
            //提取byte低4位码
            bit = bs[i] & 0x0f;
            //将这4位码对应的16进制字符添加到缓存
            sb.append(chars[bit]);
            // sb.append(' ');
        }
//        提取16进制字符串返回
        return sb.toString().trim();
    }

    /**
     * 16进制直接转换成为字符串(无需Unicode解码)
     * @param hexStr
     * @return
     */
    public static String hexStr2Str(String hexStr) {
        String str = "0123456789ABCDEF";
        //分解字符串
        //16
        char[] hexs = hexStr.toCharArray();
        //16->8 长度减半
        byte[] bytes = new byte[hexStr.length() / 2];
        int n;
        for (int i = 0; i < bytes.length; i++) {
            //获取高4位10进制码
            //hexs[2 * i]获取高4位对应字符
            //indexOf 获取字符索引位置
            n = str.indexOf(hexs[2 * i]) * 16;
            //获取低4位10进制码
            n += str.indexOf(hexs[2 * i + 1]);
            bytes[i] = (byte) (n & 0xff);
        }
        //8->16
        return new String(bytes);
    }
    /**
     * 2进制直接转换成为16进制字符串(无需Unicode解码)
     * @param
     * @return
     */
    public static String bin2hexStr(byte[] bin){
        return ( new HexBinaryAdapter().marshal(bin));
    }
    /**
     * 16进制直接转换成为2进制(无需Unicode解码)
     * @param hexStr
     * @return
     */
    public static byte[] hexStr2bin(String hexStr){
        return ( new HexBinaryAdapter().unmarshal(hexStr));
    }
}
