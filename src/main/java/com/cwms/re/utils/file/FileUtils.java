package com.cwms.re.utils.file;

import com.cwms.re.config.Global;

import org.springframework.util.ResourceUtils;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Date;

public class FileUtils {

    public static String outFile(MultipartFile multipartFile,String pack) throws IOException {
        //获取原始文件名
        String filename = multipartFile.getOriginalFilename();
        //System.out.println(filename);

        //tom.pic1.jpg

        //获取后缀名
        String suffix = filename.substring(filename.lastIndexOf(".") + 1);
        //System.out.println(suffix);
        //文件上传真实路径
        /**保存位置*/
        String realPath = null;
        if (Global.place==2)
            realPath = "//resource/";
        else if (Global.place==1){
            realPath= "/D:/CWMS/RE/SpringBoot/CWMS_RE/";
        }
        else
            realPath= ResourceUtils.getURL("classpath:").getPath();
//

        //System.out.println(realPath);
        /** filePath  = 包路径+当前时间戳+ 原文件名+后缀名*/
        String filePath="/"+pack+"/"+new Date().getTime()+filename.substring(0,filename.lastIndexOf("."))+"."+suffix;
        /** 真实路径 = 上传真实路径 + static + 包 + 时间戳 +后缀名*/

        String path=realPath.substring(1,realPath.length())+"static"+filePath;
        if(Global.printDebugMsg){
            System.out.println("源文件名："+filename);
            System.out.println("源文件名(不含后缀名)："+filename.substring(0,filename.lastIndexOf(".")));
            System.out.println("上传真实路径(类路径)："+realPath);
            System.out.println("相对路径："+filePath);
            System.out.println("绝对路径："+path);
        }
        File newFile = new File(path);


        try {
            //将接受到的文件复制给新文件
            /** 创建父目录*/
            createParentPath(newFile);
            /**保存文件*/
            multipartFile.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }

    public static String outFileAddPath(MultipartFile multipartFile,String pack,String addpath) throws IOException {
        //获取原始文件名
        String filename = multipartFile.getOriginalFilename();
        //System.out.println(filename);

        //tom.pic1.jpg

        //获取后缀名
        String suffix = filename.substring(filename.lastIndexOf(".") + 1);
        //System.out.println(suffix);
        //文件上传真实路径
        String realPath = ResourceUtils.getURL("classpath:").getPath();
        //System.out.println(realPath);
        /** 相对路径  = 包路径+当前时间戳+ 原文件名+后缀名*/
        String filePath="/"+pack+"/"+new Date().getTime()+filename.substring(0,filename.lastIndexOf("."))+"."+suffix;
        /** 真实路径 = 上传真实路径 + static + 包 + 时间戳 +后缀名*/
        String path=addpath+"static"+filePath;
        if(Global.printDebugMsg){
            System.out.println("源文件名："+filename);
            System.out.println("源文件名(不含后缀名)："+filename.substring(0,filename.lastIndexOf(".")));
            System.out.println("上传真实路径(类路径)："+realPath);
            System.out.println("相对路径："+filePath);
            System.out.println("绝对路径："+path);
        }
        File newFile = new File(path);


        try {
            //将接受到的文件复制给新文件
            /** 创建父目录*/
            createParentPath(newFile);
            /**保存文件*/
            multipartFile.transferTo(newFile);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return path;
    }


    public static void createParentPath(File file) {
        File parentFile = file.getParentFile();
        if (null != parentFile && !parentFile.exists()) {
            parentFile.mkdirs(); // 创建文件夹
            createParentPath(parentFile); // 递归创建父级目录
        }
    }

    }
