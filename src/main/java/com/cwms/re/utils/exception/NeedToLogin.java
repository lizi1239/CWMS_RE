package com.cwms.re.utils.exception;

import lombok.Data;

/**
 * 用途：
 *      用于请求没有token异常
 *      当一个请求需要token，但没有携带token时会抛出这个异常
 *
 *
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Data
public class NeedToLogin extends RuntimeException{
    public NeedToLogin(){
        super("请重新登录！！");
    }
    private int code=401;
}
