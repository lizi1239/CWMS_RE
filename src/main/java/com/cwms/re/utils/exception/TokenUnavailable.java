package com.cwms.re.utils.exception;

/**
 * 用途：
 *      用于验证token失败
 *      当用户发送请求，需要验证时，token验证不通过，会抛出这个异常
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */

public class TokenUnavailable extends RuntimeException{

    public TokenUnavailable(){
        super("效验失败");

    }

}
