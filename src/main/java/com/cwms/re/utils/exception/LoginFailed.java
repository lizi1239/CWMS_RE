package com.cwms.re.utils.exception;

import lombok.Data;

/**
 * 用途：
 *      用于登录产生异常
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Data
public class LoginFailed extends RuntimeException{
    public  LoginFailed(){
        super("登陆失败");

    }
    private int code=401;
}
