package com.cwms.re.utils.exception;

import lombok.Data;

/**
 * 用途：
 *      用于注册失败异常
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Data
public class RegFailed extends RuntimeException{
    public  RegFailed(){
        super("注册失败");
    }
    private int code=401;
}
