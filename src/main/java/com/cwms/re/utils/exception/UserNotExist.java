package com.cwms.re.utils.exception;

import lombok.Data;

/**
 * 用途：
 *      用于用户不存在
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Data
public class UserNotExist extends RuntimeException{
    public UserNotExist(){
        super("用户不存在，请重新输入");
    }
    private int code=401;
}
