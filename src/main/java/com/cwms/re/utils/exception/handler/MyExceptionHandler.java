package com.cwms.re.utils.exception.handler;



import com.cwms.re.utils.exception.*;
import lombok.Data;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
/**
 * 用途：
 *      发送异常响应
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */

@ControllerAdvice
public class MyExceptionHandler {
    @Data
    private class ErrMsg{
        private int code;
        private String msg;
    }
    /**
     * token失效
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(TokenUnavailable.class)
    public ErrMsg handleExceptionTokenUnavailable(Exception e){
        ErrMsg resultMsg  = new ErrMsg();
        resultMsg.setCode(401);
        resultMsg.setMsg(e.getMessage());
        return resultMsg;
    }

    /**
     * 需要重新登陆
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(NeedToLogin.class)
    public ErrMsg handleExceptionNeedToLogin(Exception e){
        ErrMsg resultMsg  = new ErrMsg();
        resultMsg.setCode(402);
        resultMsg.setMsg(e.getMessage());
        return resultMsg;
    }

    /**
     * 注册失败
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(RegFailed.class)
    public ErrMsg handleExceptionRegFailed(Exception e){
        ErrMsg resultMsg  = new ErrMsg();
        resultMsg.setCode(403);
        resultMsg.setMsg(e.getMessage());
        return resultMsg;
    }
    /**
     * 注册失败
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(LoginFailed.class)
    public ErrMsg handleExceptionLoginFailed(Exception e){
        ErrMsg resultMsg  = new ErrMsg();

        resultMsg.setCode(404);
        resultMsg.setMsg(e.getMessage());
        return resultMsg;
    }

    /**
     * 用户不存在
     * @param e
     * @return
     */
    @ResponseBody
    @ExceptionHandler(UserNotExist.class)
    public ErrMsg handleExceptionUserNotExist(Exception e){
        ErrMsg resultMsg  = new ErrMsg();
        resultMsg.setCode(405);
        resultMsg.setMsg(e.getMessage());
        return resultMsg;
    }


}
