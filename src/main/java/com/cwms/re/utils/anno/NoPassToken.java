package com.cwms.re.utils.anno;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;


/**
 * 用途：
 *      用于某些请求跳过token验证
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Target({ElementType.METHOD, ElementType.TYPE})
@Retention(RetentionPolicy.RUNTIME)
public @interface NoPassToken {
    boolean required() default true;
}
