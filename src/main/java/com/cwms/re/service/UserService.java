package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.User;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.result.LoginData;
import com.cwms.re.result.RegData;


/**
 * 用途：
 *
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */

public interface UserService extends IService<User> {
    public LoginData login(String account, String password, String type) throws Exception;
    public RegData reg(User regUser) throws Exception;
    public BasicMsg updataPassword(String account,String password) throws Exception;
}
