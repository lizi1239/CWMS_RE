package com.cwms.re.service;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.MyClass;
import com.cwms.re.pojo.StuClass;
import com.cwms.re.result.BasicMsg;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public interface ClassService extends IService<MyClass> {
    public BasicMsg create(String account,MyClass myClass) throws NoSuchAlgorithmException;
    public List<MyClass> getclass(String account);

}
