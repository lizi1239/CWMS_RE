package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.mapper.UploadMapper;
import com.cwms.re.pojo.CommitWork;
import com.cwms.re.result.BasicMsg;

public interface UploadService extends IService<CommitWork> {
    public BasicMsg uploading(String account,CommitWork commitWork);
}
