package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.PushWork;
import com.cwms.re.result.BasicMsg;

public interface PushWorkService extends IService<PushWork> {
    public BasicMsg push(PushWork pushWork);
}
