package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.StuClass;
import com.cwms.re.result.BasicMsg;

import java.util.List;

public interface StudentClassService extends IService<StuClass> {
    public BasicMsg insert(String account,String classId);
    public BasicMsg delete(String account,String classId);
    public List<StuClass> getclass(String account);
}
