package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.User;
import com.cwms.re.result.TeacherData;

public interface RetrieveTeacherService extends IService<User> {
    public TeacherData retrieveData (String account);
}
