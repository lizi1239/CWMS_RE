package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;

import com.cwms.re.pojo.Teacher;
import com.cwms.re.pojo.User;

import com.cwms.re.mapper.TeacherMapper;
import com.cwms.re.mapper.UserMapper;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UpdataTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UpdataTeacherServiceImpl implements UpdataTeacherService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    TeacherMapper teacherMapper;
    @Override
    public BasicMsg updataTeacherBasicData(String account,String userName, String name, String teacherId) {
        BasicMsg basicMsg  = new BasicMsg();
        if (account!=null) {
            /** 更新user表*/
            if(userMapper.getByAccount(account)!=null) {
                UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
                userUpdateWrapper.eq("account", account);
                User user = new User();

                if (userName != null) user.setUserName(userName);

                userMapper.update(user, userUpdateWrapper);
                /** 更新teacher表*/
                if(teacherMapper.getByAccount(account)!=null) {
                    UpdateWrapper<Teacher> teacherUpdateWrapper = new UpdateWrapper<>();
                    teacherUpdateWrapper.eq("account", account);

                    Teacher teacher = new Teacher();
                    if (name != null) teacher.setName(name);
                    if (teacherId != null) teacher.setTeacherId(teacherId);

                    teacherMapper.update(teacher, teacherUpdateWrapper);





                }else{
                    /**添加老师*/
                    Teacher teacher = new Teacher();
                    teacher.setTeacherId(teacherId);
                    teacher.setAccount(account);
                    teacher.setName(name);
                    teacherMapper.insert(teacher);
                }
                basicMsg.setCode(200);
                basicMsg.setMsg("信息修改成功！！");
                return basicMsg;
            }else{
                basicMsg.setCode(402);
                basicMsg.setMsg("用户不存在！！");
                return basicMsg;
            }



        }else {
            basicMsg.setCode(402);
            basicMsg.setMsg("输入用户为空！！");
            return basicMsg;
        }

    }
}
