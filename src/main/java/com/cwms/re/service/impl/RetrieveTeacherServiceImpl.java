package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.cwms.re.pojo.Teacher;
import com.cwms.re.pojo.User;

import com.cwms.re.mapper.TeacherMapper;
import com.cwms.re.mapper.UserMapper;


import com.cwms.re.result.TeacherData;
import com.cwms.re.service.RetrieveTeacherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class RetrieveTeacherServiceImpl extends ServiceImpl<UserMapper, User> implements RetrieveTeacherService {
    @Resource
    UserMapper userMapper;
    @Autowired
    TeacherMapper teacherMapper;
    @Override
    public TeacherData retrieveData(String account) {

        TeacherData teacherData = new TeacherData();
        User user = userMapper.getByAccount(account);
        Teacher teacher = teacherMapper.getByAccount(account);

        teacherData.setTeacher(teacher);
        teacherData.setUser(user);
        if(user!=null&&teacher!=null){
            teacherData.setCode("200");
            teacherData.setMsg("信息完整！");
        }else{
            teacherData.setCode("201");
            teacherData.setMsg("信息缺失！");
        }
        return teacherData;
    }

}
