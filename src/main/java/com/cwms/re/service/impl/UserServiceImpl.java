package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.config.Global;
import com.cwms.re.pojo.Student;
import com.cwms.re.pojo.Teacher;
import com.cwms.re.pojo.User;

import com.cwms.re.mapper.StudentMapper;
import com.cwms.re.mapper.TeacherMapper;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.result.LoginData;
import com.cwms.re.result.RegData;
import com.cwms.re.mapper.UserMapper;
import com.cwms.re.service.UserService;

import com.cwms.re.utils.hmac.MyHmac;
import com.cwms.re.utils.hmac.Str2Hex2Bin;
import com.cwms.re.utils.token.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**--------------------------------------------------------------------------------------------------------
 * 用途：
 *
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */

@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    StudentMapper studentMapper;
    @Resource
    TeacherMapper teacherMapper;
    @Override
    public LoginData login(String account, String password , String type) throws Exception {

        User user = userMapper.getByAccount(account);
        String verifyPassWord = null;
        if(Global.printDebugMsg){
            System.out.println("service登录中... ");
            System.out.println(user);
            System.out.println(account+password+type);

        }

//        ResultMsg resultMsg = new ResultMsg();
        LoginData loginData = new LoginData();
        if (user!=null){
            /**
             * 用户存在,生成验证密码
             * 获取盐
             */
            String sqlSalt = user.getSalt();
            /**
             * 转换盐
             */
            byte[] sqlSaltBin = Str2Hex2Bin.hexStr2bin(sqlSalt);
            /**
             * 转换原始密码
             */
            byte [] binPassWord = Str2Hex2Bin.hexStr2bin(Str2Hex2Bin.str2HexStr(password));
            /**
             * 生成验证密码
             */
            verifyPassWord = MyHmac.encodeHmacSHA256(binPassWord, sqlSaltBin);
            if(Global.printDebugMsg)
            System.out.println(verifyPassWord);

        }
        if (user !=null  && user.getAccount().equals(account) && user.getPassword().equals(verifyPassWord) && user.getType().equals(type)) {

            /**
             * 验证成功
             */
            if(Global.printDebugMsg)
            System.out.println("ok");

            //如果成功了，聚合需要返回的信息
            loginData.setAccount(account);
            loginData.setMsg("OK");
            loginData.setCode(200);
            loginData.setUserName(user.getUserName());
            loginData.setData(null);
            //给分配一个token 然后返回
            String jwtToken = Jwt.createToken(account);

            //我的处理方式是把token放到model里去了
            loginData.setToken(jwtToken);
//                response.setStatus(222);

            return loginData;
        } else {

            loginData.setMsg("用户名或密码错误");
            loginData.setCode(401);
            loginData.setData(null);
            return loginData;
        }

    }


    /**----------------------------------------------------------------------------------------------------------------
     *
     * @param regUser
     * @return
     */
    @Override
    public RegData reg(User regUser) throws Exception {
        if(Global.printDebugMsg)
            System.out.println("service注册中... ");
        User user = userMapper.getByAccount(regUser.getAccount());
        RegData regData = new RegData();
        if(user==null){
            /**
             * 用户不存在，允许注册
             */
            String password = regUser.getPassword();
            String type  =regUser.getType();
            String userName = regUser.getUserName();
            String account = regUser.getAccount();
            if(Global.printDebugMsg)
                System.out.println(password+type+userName+account);
            if(password!=null &&  type!=null && userName!=null && account!=null){
                /**
                 * 注册的用户信息完整
                 */
                /**
                 * 将密码转换为二进制
                 */
                String hexStrPassword = Str2Hex2Bin.str2HexStr(password);
                byte[] binPassword = Str2Hex2Bin.hexStr2bin(hexStrPassword);
                /**
                 * 生成随机盐
                 * 转换为HexStr形式
                 */
                byte[] salt = MyHmac.initHmacSHA256Key();

                String saltHex = Str2Hex2Bin.bin2hexStr(salt);

                /**
                 * 生成加密后的密码
                 */
                String sqlPassWord = MyHmac.encodeHmacSHA256(binPassword, salt);
                /**
                 * 生成最终注册用户 sqlUser
                 */
                User sqlUser = new User();
                sqlUser.setAccount(account);
                sqlUser.setPassword(sqlPassWord);
                sqlUser.setSalt(saltHex);
                sqlUser.setType(type);
                sqlUser.setUserName(userName);

                /**
                 * 提交用户
                 */
                userMapper.insert(sqlUser);
                if(type.equals("student")){
                    Student student = new Student();
                    student.setAccount(account);
                    student.setStuId(account);
                    student.setName(userName);
                    /**
                     * 将学生添加到学生表
                     */
                    studentMapper.insert(student);
                }else {
                    Teacher teacher  = new Teacher();
                    teacher.setAccount(account);
                    teacher.setName(userName);
                    teacher.setTeacherId(account);
                    /**
                     * 将教师填入教师表
                     */
                    teacherMapper.insert(teacher);
                }
                user = userMapper.getByAccount(account);
                if(user.getAccount().equals(account)){
                    /**
                     * 查询到新用户,添加成功
                     */
                    regData .setCode(200);
                    regData .setMsg("成功添加用户"+account);
                    regData .setUserName(user.getUserName());
                    regData .setAccount(account);
                    return regData ;
                }else{

                    regData .setCode(403);
                    regData .setMsg("添加失败，请再次添加！");

                    return regData ;
                }



/**
 *              解密过程

                byte[] sqlSaltBin = Str2Hex2Bin.hexStr2bin(saltHex);
                sqlPassWord = MyHmac.encodeHmacSHA256(binPassword, sqlSaltBin);
                System.out.println("第2次加密的sql密码："+sqlPassWord);
 */

            }else {

                regData .setCode(403);
                regData .setMsg("添加失败，信息不全！");

                return regData ;
            }

        }else{
            /**
             * 用户已存在，返回错误信息
             */

            regData .setCode(403);
            regData .setMsg("添加失败，用户已存在！");

            return regData ;
        }


    }


    @Override
    public BasicMsg updataPassword(String account,String password) throws Exception {
        BasicMsg basicMsg = new BasicMsg();
        User user = userMapper.getByAccount(account);
        if (user != null) {
            /** 更新条件*/
            UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
            userUpdateWrapper.eq("account", account);
            /** 更新为*/
            /**将初始密码转换为十六与二进制*/
            String hexStrPassword = Str2Hex2Bin.str2HexStr(password);
            byte[] binPassword = Str2Hex2Bin.hexStr2bin(hexStrPassword);
            /**生成随机盐 转换为HexStr形式*/
            byte[] salt = MyHmac.initHmacSHA256Key();
            String saltHex = Str2Hex2Bin.bin2hexStr(salt);
            /**生成加密后的密码*/
            String sqlPassWord = MyHmac.encodeHmacSHA256(binPassword, salt);
            User updataUser = new User();
            updataUser.setPassword(sqlPassWord);
            updataUser.setSalt(saltHex);
            userMapper.update(updataUser, userUpdateWrapper);
            /** 更新成功*/
            if (userMapper.getByAccount(account).getPassword().equals(sqlPassWord)) {
                basicMsg.setMsg("密码更新成功！！");
                basicMsg.setCode(200);
                return basicMsg;
            }
            /**不成功，再次更新*/
            else {
                return updataPassword(account, password);
            }
        } else {
            basicMsg.setCode(402);
            basicMsg.setMsg("用户不存在！！");
            return basicMsg;
        }

    }


}
