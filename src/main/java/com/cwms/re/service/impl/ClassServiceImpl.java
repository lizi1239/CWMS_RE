package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.config.Global;

import com.cwms.re.mapper.ClassMapper;
import com.cwms.re.mapper.TeacherMapper;
import com.cwms.re.pojo.MyClass;
import com.cwms.re.pojo.Student;
import com.cwms.re.pojo.Teacher;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.ClassService;
import com.cwms.re.utils.hmac.InitId;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Random;

@Service
public class ClassServiceImpl extends ServiceImpl<ClassMapper, MyClass> implements ClassService {
    @Resource
    ClassMapper classMapper;
    @Resource
    TeacherMapper teacherMapper;
    @Override
    public BasicMsg create(String account,MyClass insertClass) {
        BasicMsg basicMsg = new BasicMsg();
        String classId  = initClassId();

        if(Global.printDebugMsg){
            System.out.println(classId.length());

            System.out.println(classId);
        }
        /**
         * 获得64位的随机ID，截取12位
         */
        /** 查询班课是否存在*/
        QueryWrapper<MyClass> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("class_id",classId);
        MyClass myClass = classMapper.selectOne(queryWrapper);


        if(myClass == null){
            Teacher teacher= teacherMapper.getByAccount(account);
            String teacherId =teacher.getTeacherId();
            String teacherName = teacher.getName();
            if(teacher!=null){

                insertClass.setClassId(classId);
                insertClass.setTeacherId(teacherId);
                insertClass.setTeacherName(teacherName);
                int insert = classMapper.insert(insertClass);
                if (insert!=0){
                    basicMsg.setCode(200);
                    basicMsg.setMsg("创建成功");
                    basicMsg.setData(insertClass);

                    return basicMsg;
                }
            }

        }
        basicMsg.setCode(404);
        basicMsg.setMsg("创建失败");


        return basicMsg;
    }

    @Override
    public List<MyClass> getclass(String account) {
        Teacher teacher = teacherMapper.getByAccount(account);
        if (teacher!=null){
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("teacher_id", teacher.getTeacherId());
            return  classMapper.selectList(queryWrapper);
        }

        return null;
    }

    private String initClassId(){
        return InitId.init64Id().substring(0,12);
    }
}

