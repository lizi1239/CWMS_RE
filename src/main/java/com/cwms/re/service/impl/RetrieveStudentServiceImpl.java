package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.pojo.Student;
import com.cwms.re.pojo.User;
import com.cwms.re.mapper.StudentMapper;
import com.cwms.re.mapper.UserMapper;
import com.cwms.re.result.StudentData;
import com.cwms.re.service.RetrieveStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RetrieveStudentServiceImpl extends ServiceImpl<UserMapper, User> implements RetrieveStudentService {
    @Autowired
    UserMapper userMapper;
    @Autowired
    StudentMapper studentMapper;
    @Override
    public StudentData retrieveData(String account) {
        StudentData studentData = new StudentData();
        User user = userMapper.getByAccount(account);
        Student student = studentMapper.getByAccount(account);
        studentData.setStudent(student);
        studentData.setUser(user);
        if(user!=null&&student!=null){
            studentData.setCode("200");
            studentData.setMsg("信息完整！");
        }else{
            studentData.setCode("201");
            studentData.setMsg("信息缺失！");
        }
        return studentData;
    }
}
