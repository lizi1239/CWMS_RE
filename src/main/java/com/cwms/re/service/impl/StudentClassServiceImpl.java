package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.config.Global;

import com.cwms.re.mapper.ClassMapper;
import com.cwms.re.mapper.StudentClassMapper;
import com.cwms.re.mapper.StudentMapper;
import com.cwms.re.pojo.MyClass;
import com.cwms.re.pojo.StuClass;
import com.cwms.re.pojo.Student;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.StudentClassService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class StudentClassServiceImpl extends ServiceImpl<StudentClassMapper, StuClass> implements StudentClassService {
    @Resource
    ClassMapper classMapper;
    @Resource
    StudentClassMapper studentClassMapper;
    @Resource
    StudentMapper studentMapper;
    public List<StuClass> getclass(String account){
        Student student = studentMapper.getByAccount(account);
        if (student!=null){
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("stu_id", student.getStuId());
            return  studentClassMapper.selectList(queryWrapper);
        }

        return null;
    }
    @Override
    public BasicMsg insert(String account, String classId) {
        BasicMsg basicMsg = new BasicMsg();
        if (account!=null&&classId!=null){
            /** 查询学生是否存在*/
            Student student = studentMapper.getByAccount(account);
            /** 查询是否以在班课内 */
            QueryWrapper<StuClass> stuClassQueryWrapper  = new QueryWrapper<>();
            stuClassQueryWrapper.eq("stu_id",student.getStuId());
            stuClassQueryWrapper.eq("class_id",classId);
            StuClass stuClass1 = studentClassMapper.selectOne(stuClassQueryWrapper);
            if (stuClass1!=null){
                /** 已存在*/
                basicMsg.setCode(200);
                basicMsg.setMsg("已存在");

                return basicMsg;
            }
            /** 查询班课是否存在*/
            QueryWrapper<MyClass> queryWrapper = new QueryWrapper<>();
            queryWrapper.eq("class_id",classId);
            MyClass myClass = classMapper.selectOne(queryWrapper);


//            MyClass myClass = classMapper.selectById(classId);

            if (Global.printDebugMsg){
                System.out.println(student);
                System.out.println(myClass);
                System.out.println(classId);
            }
            if (myClass!=null&&student!=null){
                /** 存在*/
                StuClass stuClass = new StuClass();
                stuClass.setClassId(classId);
                stuClass.setStuId(student.getStuId());
                stuClass.setClassName(myClass.getClassName());
                stuClass.setTeacherName(myClass.getTeacherName());
                stuClass.setStudentName(student.getName());
                int insert = studentClassMapper.insert(stuClass);
                if (insert!=0){
                    basicMsg.setCode(200);
                    basicMsg.setMsg("添加成功");
                    basicMsg.setData(myClass);
                    return basicMsg;
                }
            }

        }
        if (Global.printDebugMsg)
        System.out.println("添加失败");
        basicMsg.setCode(500);
        basicMsg.setMsg("添加失败");
        basicMsg.setData(null);
        return basicMsg;
    }

    @Override
    public BasicMsg delete(String account, String classId) {
        BasicMsg basicMsg = new BasicMsg();
        /** 查询学生是否存在*/
        Student student = studentMapper.getByAccount(account);
        /** 查询是否以在班课内 */
        QueryWrapper<StuClass> stuClassQueryWrapper  = new QueryWrapper<>();
        stuClassQueryWrapper.eq("stu_id",student.getStuId());
        stuClassQueryWrapper.eq("class_id",classId);
        StuClass stuClass1 = studentClassMapper.selectOne(stuClassQueryWrapper);
        if (stuClass1!=null){
            QueryWrapper<StuClass> queryWrapper = new QueryWrapper<>();

            int delete = studentClassMapper.delete(queryWrapper);
            if(delete!=0){
                /** 删除成功*/
                basicMsg.setCode(200);
                basicMsg.setMsg("删除成功");
                return basicMsg;
            }
        }else{
            basicMsg.setCode(404);
            basicMsg.setMsg("删除失败,未在这个班课内");
            return basicMsg;
        }
        basicMsg.setCode(500);
        basicMsg.setMsg("删除失败");
        return basicMsg;
    }
}
