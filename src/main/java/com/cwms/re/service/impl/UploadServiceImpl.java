package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.mapper.PushWorkMapper;
import com.cwms.re.mapper.StudentMapper;
import com.cwms.re.mapper.UploadMapper;
import com.cwms.re.pojo.CommitWork;
import com.cwms.re.pojo.PushWork;
import com.cwms.re.pojo.Student;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UploadService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
@Service
public class UploadServiceImpl extends ServiceImpl<UploadMapper, CommitWork> implements UploadService {
    @Resource
    UploadMapper uploadMapper;
    @Resource
    StudentMapper studentMapper;
    @Resource
    PushWorkMapper pushWorkMapper;
    @Override
    public BasicMsg uploading(String account, CommitWork commitWork) {
        BasicMsg basicMsg = new BasicMsg();
        Student student = studentMapper.getByAccount(account);
        if(student!=null){
            /** 学生存在*/
            commitWork.setStuId(student.getStuId());
            /** 查询是否存在父作业*/
            QueryWrapper queryWrapper = new QueryWrapper();
            queryWrapper.eq("work_id",commitWork.getWorkId());
            queryWrapper.eq("class_id",commitWork.getClassId());

            PushWork pushWork = pushWorkMapper.selectOne(queryWrapper);
            if(pushWork!=null){
                /** 父作业存在*/
                commitWork.setWorkName(student.getName()+" 的 "+ pushWork.getWorkName());
                int insert = uploadMapper.insert(commitWork);
                if(insert!=0){
                    basicMsg.setCode(200);
                    basicMsg.setMsg("提交成功");
                    basicMsg.setData(commitWork);
                    return basicMsg;
                }
            }
        }
        basicMsg.setCode(500);
        basicMsg.setMsg("失败，请检查");

        return basicMsg;
    }
}
