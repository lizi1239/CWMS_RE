package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.cwms.re.mapper.PushWorkMapper;
import com.cwms.re.pojo.PushWork;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.PushWorkService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class PushWorkServiceImpl extends ServiceImpl<PushWorkMapper, PushWork> implements PushWorkService {
    @Resource
    PushWorkMapper pushWorkMapper;
    @Override
    public BasicMsg push(PushWork pushWork) {
        BasicMsg basicMsg = new BasicMsg();
        int insert = pushWorkMapper.insert(pushWork);
        if (insert != 0){
            basicMsg.setCode(200);
            basicMsg.setMsg("成功");
            basicMsg.setData(pushWork);
            return basicMsg;
        }
        basicMsg.setCode(500);
        basicMsg.setMsg("失败");
        return basicMsg;
    }
}
