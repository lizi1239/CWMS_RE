package com.cwms.re.service.impl;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.cwms.re.pojo.Student;
import com.cwms.re.pojo.User;
import com.cwms.re.mapper.StudentMapper;
import com.cwms.re.mapper.UserMapper;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UpdataStudentService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class UpdataStudentServiceImpl implements UpdataStudentService {
    @Resource
    UserMapper userMapper;
    @Resource
    StudentMapper studentMapper;
    @Override
    public BasicMsg updataStudentBasicData(String account,String userName, String name, String strId) {
        BasicMsg basicMsg  = new BasicMsg();
        if (account!=null) {
            /** 更新user表*/
            if(userMapper.getByAccount(account)!=null) {
                UpdateWrapper<User> userUpdateWrapper = new UpdateWrapper<>();
                userUpdateWrapper.eq("account", account);
                User user = new User();

                if (userName != null) user.setUserName(userName);

                userMapper.update(user, userUpdateWrapper);
                /** 更新student表*/
                if(studentMapper.getByAccount(account)!=null) {
                    UpdateWrapper<Student> studentUpdateWrapper = new UpdateWrapper<>();
                    studentUpdateWrapper.eq("account", account);
                    Student student = new Student();

                    if (name != null) student.setName(name);
                    if (strId != null) student.setStuId(strId);

                    studentMapper.update(student, studentUpdateWrapper);
                }else{
                    /**添加学生*/
                    Student student = new Student();
                    student.setStuId(strId);
                    student.setAccount(account);
                    student.setName(name);
                    studentMapper.insert(student);
                }
                basicMsg.setCode(200);
                basicMsg.setMsg("信息修改成功！！");
                return basicMsg;
            }else{
                basicMsg.setCode(402);
                basicMsg.setMsg("用户不存在！！");
                return basicMsg;
            }



        }else {
            basicMsg.setCode(402);
            basicMsg.setMsg("输入用户为空！！");
            return basicMsg;
        }

    }
}
