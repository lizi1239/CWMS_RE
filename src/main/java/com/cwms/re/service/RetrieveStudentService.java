package com.cwms.re.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.cwms.re.pojo.User;
import com.cwms.re.result.StudentData;

public interface RetrieveStudentService extends IService<User> {
    public StudentData retrieveData (String account);
}
