package com.cwms.re.service;

import com.cwms.re.result.BasicMsg;

public interface UpdataTeacherService {
    public BasicMsg updataTeacherBasicData(String account, String userName, String name, String teacherId);
}
