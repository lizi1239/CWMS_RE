package com.cwms.re.config.interceptor.handler;



import com.cwms.re.config.Global;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.exception.NeedToLogin;
import com.cwms.re.utils.token.Jwt;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;
/**
 * 用途：
 *      对除了有NoPassToken外的所有请求进行token验证
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
public class JwtAuthenticationInterceptor implements HandlerInterceptor {

    @Autowired
    UserService userService;

    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object object) throws Exception {
        // 从请求头中取出 token  这里需要和前端约定好把jwt放到请求头一个叫token的地方
        String token = httpServletRequest.getHeader("token");
        // 如果不是映射到方法直接通过
        if (!(object instanceof HandlerMethod)) {
            return true;
        }
        HandlerMethod handlerMethod = (HandlerMethod) object;
        Method method = handlerMethod.getMethod();

        //检查是否有nopasstoken注释，有则跳过认证
        if (method.isAnnotationPresent(NoPassToken.class)) {
            if(Global.printDebugMsg)
                System.out.println("没被拦截");
            NoPassToken noPassToken = method.getAnnotation(NoPassToken.class);
            if (noPassToken.required()) {
                return true;
            }
        }
        //默认全部检查
        else {
            if(Global.printDebugMsg)
                System.out.println("被jwt拦截需要验证");
            // 执行认证
            if (token == null) {
                //没token   这个错误也是我自定义的，读者需要自己修改
                throw new NeedToLogin();
            }

            // 获取 token 中的 user acount
            String account = Jwt.getAudience(token);

            //找找看是否有这个user   因为我们需要检查用户是否存在，读者可以自行修改逻辑
//            AccountDTO user = accountService.getByUserName(userId);

            /**
             * 这里不合理我们设置token的目的就是为了在验证用户的同时减少服务器资源的开销
             * 如果每个请求都需要进行一次数据库访问，那么token将毫无意义
             *
             */

//            User user = userService.getById(userId);
//
//            if (user == null) {
//                //这个错误也是我自定义的
//                throw new UserNotExist();
//            }

            // 验证 token
            Jwt.verifyToken(token, account);

            //获取载荷内容
//            String userName = JwtUtils.getClaimByName(token, "username").asString();
//            String realName = JwtUtils.getClaimByName(token, "realName").asString();

            //获取签发对象
            String userName = Jwt.getAudience(token);
            //放入attribute以便后面调用


            httpServletRequest.setAttribute("userName", userName);
//            httpServletRequest.setAttribute("realName", realName);


            return true;

        }
        return true;
    }

    @Override
    public void postHandle(HttpServletRequest httpServletRequest,
                           HttpServletResponse httpServletResponse,
                           Object o, ModelAndView modelAndView) throws Exception {

    }

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest,
                                HttpServletResponse httpServletResponse,
                                Object o, Exception e) throws Exception {
    }

}
