package com.cwms.re.config.interceptor;


import com.cwms.re.config.interceptor.handler.JwtAuthenticationInterceptor;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * 用途：
 *      拦截所有请求，对所有请求进行token验证处理
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
/*
 *实现WebMvcConfigurer配置类接口
 * 用于解决登录问题
 *@Configuration标明这是一个配置类
 * */
@Configuration
public class JwtInterceptorConfig implements WebMvcConfigurer {
    /*
     *重写拦截器配置：addInterceptors
     *
     */
    @Override
    public void addInterceptors(InterceptorRegistry registry) {

        //默认拦截所有路径
        //添加拦截器--addInterceptor
        //registry注册
        registry.addInterceptor(authenticationInterceptor())
                .addPathPatterns("/**");
    }
    //处理规则
    @Bean
    public JwtAuthenticationInterceptor authenticationInterceptor() {

        //对token进行验证
        return new JwtAuthenticationInterceptor();
    }

}
