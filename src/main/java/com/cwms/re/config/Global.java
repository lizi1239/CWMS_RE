package com.cwms.re.config;

import lombok.Data;
import org.springframework.stereotype.Component;

@Data
@Component
public class Global {
    public static Boolean printDebugMsg = true;
    /**0.类路径 1.自定义windows 2.自定义linux*/
    public static int place = 2;

}

