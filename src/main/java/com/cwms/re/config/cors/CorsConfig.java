package com.cwms.re.config.cors;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
/**
 * 用途：
 *      解决跨域问题
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
/*
 *实现WebMvcConfigurer配置类接口
 * 用于处理跨域问题
 *
 * */
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    /*
     * 配置全局跨域请求
     *
     *
     * */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/cors/**").
                allowedHeaders("*").
                allowedMethods("*").
                maxAge(1800).
                allowedOrigins("*");

        registry.addMapping("/**").
                allowedHeaders("*").
                allowedMethods("*").
                maxAge(1800).
                allowedOrigins("*");
    }

}
