package com.cwms.re.controller;

import com.cwms.re.config.Global;

import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UpdataStudentService;
import com.cwms.re.utils.token.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("updata")
@ResponseBody
public class UpdataStudentDataController {
    @Autowired
    UpdataStudentService updataStudentService;

    @PostMapping(value = "/student")
    public BasicMsg updataStudentBasicData(@RequestBody Map jsonMap, HttpServletRequest request){
        String token = request.getHeader("token");
        String account = Jwt.getAudience(token);
        String userName = (String) jsonMap.get("userName");
        String name = (String) jsonMap.get("name");
        String strId = (String) jsonMap.get("strId");
        if(Global.printDebugMsg){

            System.out.println("Controller更新学生基本信息！！");
//            System.out.println("token is "+token);
            System.out.println("acount is "+account);
            System.out.println("userName is "+userName);
            System.out.println("name is "+name);
            System.out.println("strId is "+strId);

        }
        BasicMsg basicMsg;
        basicMsg = updataStudentService.updataStudentBasicData(account,userName,name,strId);
        return basicMsg;
    }
}
