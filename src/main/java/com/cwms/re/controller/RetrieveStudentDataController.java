package com.cwms.re.controller;

import com.cwms.re.config.Global;

import com.cwms.re.result.StudentData;
import com.cwms.re.service.RetrieveStudentService;
import com.cwms.re.utils.token.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用于获取学生信息
 */
@Controller
@RequestMapping("retrieve")
@ResponseBody
public class RetrieveStudentDataController {

    @Autowired
    RetrieveStudentService retrieveStudentService;

    /**
     * 在get请求中@RequestBody 并不适用
     * @param httpServletRequest
     * @param httpServletResponse
     * @return
     */
    @GetMapping(value = "/student")
    public StudentData Retrieve(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        // 从请求头中取出 token  这里需要和前端约定好把jwt放到请求头一个叫token的地方
        String token = httpServletRequest.getHeader("token");
        String account = Jwt.getAudience(token);
        StudentData studentData;
        if(Global.printDebugMsg)
        System.out.println("Controller获取学生基本信息！！");
        if(account!=null){
          studentData =  retrieveStudentService.retrieveData(account);
           if (studentData!=null){
               return studentData;
           }else{
               studentData = new StudentData();
               studentData.setCode("402");
               studentData.setMsg("用户不存在");
               return studentData;
           }
        }else{
            studentData = new StudentData();
            studentData.setCode("402");
            studentData.setMsg("用户不存在");
            return studentData;
        }

    }
}
