package com.cwms.re.controller;

import com.cwms.re.config.Global;
import com.cwms.re.pojo.User;

import com.cwms.re.result.RegData;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.exception.LoginFailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
//@RequestMapping("user")
@ResponseBody
public class UserReg {

    @Autowired
    UserService userService;

    /**--------------------------------------------------------------------------------------------------------------------
     *普通用户注册接口
     *
     * 前端使用$.ajax的话，一定要指定 contentType: "application/json;charset=utf-8;"，默认为 application/x-www-form-urlencoded
     * @param jsonMap
     * @return
     * @throws LoginFailed
     */

    @NoPassToken//免token验证注解
    @PostMapping(value = "/user_reg")
    public RegData reg(@RequestBody Map jsonMap) throws Exception {
        if(Global.printDebugMsg)
        System.out.println("Controller注册中！！");
        User user = new User();
        user.setAccount((String) jsonMap.get("account"));
        user.setPassword((String) jsonMap.get("password"));
        user.setType((String) jsonMap.get("type"));
        user.setUserName((String) jsonMap.get("userName"));
        if(user.getType().equals("student")||user.getType().equals("teacher")){
            RegData regData  =  userService.reg(user);
            return regData;
        }
        else {
            RegData regData  = new RegData();
            regData .setCode(401);
            regData .setMsg("没有权限");
            return regData ;
        }


    }



}
