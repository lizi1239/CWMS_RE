package com.cwms.re.controller;

import ch.qos.logback.core.net.SyslogOutputStream;

import com.cwms.re.config.Global;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.token.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Controller
@RequestMapping("updata")
@ResponseBody
public class UpdataPasswordController {
    @Autowired
    UserService userService;

    @PostMapping(value = "/password")
    public BasicMsg updataPassword(@RequestBody Map jsonMap , HttpServletRequest request) throws Exception {
        String token = request.getHeader("token");
        String account = Jwt.getAudience(token);
        String password = (String)jsonMap.get("password");
        if(Global.printDebugMsg){

            System.out.println("Controller更新用户密码！！");
//            System.out.println("token is "+token);
            System.out.println("acount is "+account);
            System.out.println("password is "+password);
        }

        if(password!=null){
           return userService.updataPassword(account,password);
        }else{
            BasicMsg basicMsg = new BasicMsg();
            basicMsg.setCode(402);
            basicMsg.setMsg("提交的密码为空，错误！！");
            return basicMsg;
        }

    }
}
