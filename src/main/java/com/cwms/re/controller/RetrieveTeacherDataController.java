package com.cwms.re.controller;

import com.cwms.re.config.Global;

import com.cwms.re.result.TeacherData;

import com.cwms.re.service.RetrieveTeacherService;
import com.cwms.re.utils.token.Jwt;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 用于获取学生信息
 */
@Controller
@RequestMapping("retrieve")
@ResponseBody
public class RetrieveTeacherDataController {

    @Autowired
    RetrieveTeacherService retrieveTeacherService;
    @GetMapping(value = "/teacher")
    public TeacherData Retrieve(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse){
        // 从请求头中取出 token  这里需要和前端约定好把jwt放到请求头一个叫token的地方
        String token = httpServletRequest.getHeader("token");
        String account = Jwt.getAudience(token);
        TeacherData teacherData;
        if(Global.printDebugMsg)
            System.out.println("Controller获取老师基本信息！！");
        if(account!=null){
            teacherData =  retrieveTeacherService.retrieveData(account);
            if (teacherData!=null){
                return teacherData;
            }else{
                teacherData = new TeacherData();
                teacherData.setCode("402");
                teacherData.setMsg("用户不存在");
                return teacherData;
            }
        }else{
            teacherData = new TeacherData();
            teacherData.setCode("402");
            teacherData.setMsg("用户不存在");
            return teacherData;
        }

    }
}
