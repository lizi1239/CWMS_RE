package com.cwms.re.controller;
/**
 * 用途：
 *
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
import com.cwms.re.config.Global;
import com.cwms.re.pojo.User;

import com.cwms.re.result.LoginData;
import com.cwms.re.result.RegData;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.exception.LoginFailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Map;

@Controller
@RequestMapping("user")
@ResponseBody
public class UserController {

    @Autowired
    UserService userService;




    /**----------------------------------------------------------------------------------------------------------------
     * 用户登录：获取账号密码并登录，如果不对就报错，对了就返回用户的登录信息
     * 同时生成jwt返回给用户
     *
     * @return
     * @throws LoginFailed  这个LoginFailed也是我自定义的
     */
    @NoPassToken//免token验证注解
    @PostMapping(value = "/login")
    public LoginData login(@RequestBody Map jsonMap)  throws LoginFailed {

        String account = (String) jsonMap.get("account");
        String password = (String) jsonMap.get("password");
        String type = (String) jsonMap.get("type");
        try{
//            ResultMsg resultMsg  = null;
            LoginData loginData = null;
            if(Global.printDebugMsg)
            System.out.println("controller登录中...");

            loginData = userService.login(account,password,type);
             return loginData;
        }
        catch (Exception e)
        {
            if(Global.printDebugMsg)
            System.out.println("登录失败！！！");
            throw new LoginFailed();
        }

    }


    /**--------------------------------------------------------------------------------------------------------------------
     *普通用户注册接口
     * @param jsonMap
     * @return
     * @throws LoginFailed
     */

    @NoPassToken//免token验证注解
    @PostMapping(value = "/reg")
    public RegData reg(@RequestBody Map jsonMap) throws Exception {
        System.out.println("reg");
        User user = new User();
        user.setAccount((String) jsonMap.get("account"));
        user.setPassword((String) jsonMap.get("password"));
        user.setType((String) jsonMap.get("type"));
        user.setUserName((String) jsonMap.get("userName"));
        if(user.getType().equals("student")||user.getType().equals("teacher")){
            RegData regData  =  userService.reg(user);
            return regData;
        }
        else {
            RegData regData  = new RegData();
            regData .setCode(401);
            regData .setMsg("没有权限");
            return regData ;
        }

    }





    /**-------------------------------------------------------------------------------------------------------------------------
     * 测试没有NoPassToken，验证是否生效
     * @return
     */
    @GetMapping("getdata")
    public String testGetData(){
        return"hello axios";
    }

}
