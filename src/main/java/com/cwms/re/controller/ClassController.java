package com.cwms.re.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cwms.re.config.Global;

import com.cwms.re.pojo.MyClass;
import com.cwms.re.pojo.StuClass;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.ClassService;
import com.cwms.re.service.StudentClassService;
import com.cwms.re.utils.token.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.security.NoSuchAlgorithmException;
import java.util.List;

@RequestMapping("class")
@ResponseBody
@Controller
public class ClassController {

    @Resource
    ClassService classService;
    @Resource
    StudentClassService studentClassService;

    @GetMapping("/delete")
    public BasicMsg delete(@RequestParam  String classId){
        System.out.println("删除班级"+classId);
        QueryWrapper<StuClass> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("class_id",classId);
//        //            删除班课中的学生
//
        boolean remove1 = studentClassService.remove(queryWrapper);
        //删除班课中的作业  没实现

//        删除班课
        QueryWrapper<MyClass> queryWrapper1 = new QueryWrapper<>();
        queryWrapper1.eq("class_id",classId);
        boolean remove = classService.remove(queryWrapper1);

        BasicMsg basicMsg = new BasicMsg();
        if (remove){

            basicMsg.setCode(200);
            basicMsg.setMsg("删除成功");
            return basicMsg;
        }
        basicMsg.setCode(400);
        basicMsg.setMsg("删除失败");
        return basicMsg;
    }

    @GetMapping("/get")
    public List<MyClass> getclass(HttpServletRequest request){
        String account = Jwt.getAudience(request.getHeader("token"));


        return classService.getclass(account);
    }


    @PostMapping("/create")

    public BasicMsg create(@RequestBody MyClass myClass, HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("token");
        String acount = Jwt.getAudience(token);
        if (Global.printDebugMsg){
            System.out.println("create  Class");
            System.out.println(myClass);
//            System.out.println(className);
        }
        try {

            return classService.create(acount,myClass);
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            BasicMsg basicMsg = new BasicMsg();
            basicMsg.setMsg("创建失败");
            basicMsg.setCode(404);
            return basicMsg;
        }

    }
}
