package com.cwms.re.controller;

import com.cwms.re.config.Global;

import com.cwms.re.result.LoginData;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.exception.LoginFailed;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
//@RequestMapping("userlogin")
@ResponseBody
public class UserLogin {
    @Autowired
    UserService userService;

    /**----------------------------------------------------------------------------------------------------------------
     * 用户登录：获取账号密码并登录，如果不对就报错，对了就返回用户的登录信息
     * 同时生成jwt返回给用户
     *  将登录功能分离出来
     * @return
     * @throws LoginFailed  这个LoginFailed也是我自定义的
     */
    @NoPassToken//免token验证注解
    @PostMapping(value = "/user_login")
    public LoginData login(@RequestBody Map jsonMap)  throws LoginFailed {

        String account = (String) jsonMap.get("account");
        String password = (String) jsonMap.get("password");
        String type = (String) jsonMap.get("type");
        try{

            LoginData loginData ;
            if(Global.printDebugMsg)
            System.out.println("controller登录中...");

            loginData = userService.login(account,password,type);
            return loginData;
        }
        catch (Exception e)
        {
            System.out.println("登录失败！！！");
            throw new LoginFailed();
        }

    }


}
