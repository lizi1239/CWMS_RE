package com.cwms.re.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cwms.re.config.Global;

import com.cwms.re.pojo.PushWork;
import com.cwms.re.pojo.User;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.PushWorkService;
import com.cwms.re.service.UserService;
import com.cwms.re.utils.anno.NoPassToken;
import com.cwms.re.utils.file.FileUtils;
import com.cwms.re.utils.hmac.InitId;
import com.cwms.re.utils.token.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.annotation.Resource;
import javax.servlet.ServletRegistration;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.net.URLEncoder;
import java.sql.SQLOutput;
import java.util.List;

@Controller
@ResponseBody
@RequestMapping("work")
public class PushWorkController {
    @Resource
    PushWorkService pushWorkService;
    @Resource
    UserService userService;

    @GetMapping("/delete")
    public BasicMsg delete(String workId){
        System.out.println("删除发布的作业"+workId);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("work_id",workId);
        boolean remove = pushWorkService.remove(queryWrapper);
        BasicMsg basicMsg = new BasicMsg();
        if (remove){
            basicMsg.setCode(200);
            basicMsg.setMsg("删除成功");
            return basicMsg;
        }
        basicMsg.setCode(400);
        basicMsg.setMsg("删除失败");
        return basicMsg;
    }
    @GetMapping("/get")
    public List<PushWork> get(@RequestParam String classId){
        System.out.println("work get" + classId);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("class_id",classId);
        pushWorkService.list(queryWrapper);
        return pushWorkService.list(queryWrapper);
    }


    @PostMapping("/push")
//    @RequestParam("file")  MultipartFile multipartFile,@RequestParam("name") String fileName
//简化    MultipartFile file,String name
// 多文件 List<MultipartFile> file
    /** 存在漏洞，没有对用户进行身份验证*/
    public BasicMsg push(MultipartFile file, String name, String classId, HttpServletRequest httpServletRequest) throws IOException {

        /** 在前端，发送请求时，Content-Type(内容类型)是multipart/form-data(多部分表单)，专门用于传输二进制文件,使用boundary边界分割*/
        /** RequestBody处理application/json数据
         * MultipartFile处理multipart/form-data数据，不能为空
         * 不能同时存在*/
        String account  = Jwt.getAudience(httpServletRequest.getHeader("token"));

        if (Global.printDebugMsg){
            System.out.println("push");

            System.out.println("name: "+name);
            System.out.println("file: "+file.getOriginalFilename());

        }
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("account",account);
        User user = userService.getOne(queryWrapper);
        /** 是老师，可以添加*/
        if (user.getType().equals("teacher")){

            /** 保存文件*/
            String path = FileUtils.outFile(file,"push_work");
            String fileName = file.getOriginalFilename();
            if (name==null){
                name = fileName.substring(0,fileName.lastIndexOf("."));
            }

            String workId = InitId.init64Id().substring(0,12);


//
//        FileUtils.outFileAddPath(file,"push_work","D:/CWMS/RE/SpringBoot/CWMS_RE/");
//        private String classId;
//        private String workId;
//        private String path;
//        private String wrodName;
            /** 初始化作业*/
            PushWork pushWork= new PushWork();
            pushWork.setClassId(classId);
            pushWork.setWorkId(workId);
            pushWork.setPath(path);
            pushWork.setWorkName(name);

            return pushWorkService.push(pushWork);
        }
        BasicMsg basicMsg = new BasicMsg();
        basicMsg.setCode(404);
        basicMsg.setMsg("权限错误");

        return basicMsg;
    }
    @NoPassToken
    @CrossOrigin
    @GetMapping("/download")
    public void download(@RequestParam String workId, HttpServletResponse response) throws IOException {
        System.out.println("下载发布的作业"+workId);
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("work_id",workId);
        PushWork pushWork = pushWorkService.getOne(queryWrapper);
        if (pushWork!=null){
            /** 获取路径*/
            System.out.println("获取路径");
            String path = pushWork.getPath();
            /** 创建文件对象*/
            File file = new File(path);
            /**获取文件名*/
            String fileName = file.getName();
            /**获取后缀名（小写）*/
            String ext = fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
            /**建立文件输入流*/
            FileInputStream fileInputStream = new FileInputStream(file);
            /** 建立缓冲流*/
            InputStream inputStream = new BufferedInputStream(fileInputStream);
            /**根据流的长度创建byte数组对象*/
            byte[] buffer = new byte[inputStream.available()];
            /**将流读取到数组中*/
            inputStream.read(buffer);
            /**关闭流*/
            inputStream.close();
            /**清空response*/
            response.reset();
            /**设置response头*/
            response.setCharacterEncoding("UTF-8");
            response.setHeader("Access-Control-Allow-Origin", "*"); //  这里最好明确的写允许的域名
            response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
            response.setHeader("Access-Control-Max-Age", "3600");
            response.setHeader("Access-Control-Allow-Headers", "Content-Type,Access-Token,Authorization,ybg");
            //解决跨域问题获取不到的头
            response.setHeader("Access-Control-Expose-Headers", "Content-Disposition");

            //Content-Disposition的作用：告知浏览器以何种方式显示响应返回的文件，用浏览器打开还是以附件的形式下载到本地保存
            //attachment表示以附件方式下载   inline表示在线打开   "Content-Disposition: inline; filename=文件名.mp3"
            // filename表示文件的默认名称，因为网络传输只支持URL编码的相关支付，因此需要将文件名URL编码后进行传输,前端收到后需要反编码才能获取到真正的名称
            response.addHeader("Content-Disposition", "attachment;filename=" + URLEncoder.encode(fileName, "UTF-8"));
            /**告知浏览器文件大小*/
            response.addHeader("Content-Length", "" + file.length());
            response.addHeader("test", "????");
            System.out.println(file.length());
            /**获取response的输出流*/
            OutputStream outputStream = new BufferedOutputStream(response.getOutputStream());
            /**设置ContentType*/
            response.setContentType("application/octet-stream");
            /**写入流*/
            System.out.println("写入流");

            outputStream.write(buffer);
            System.out.println("清空流");
            outputStream.flush();
            if (Global.printDebugMsg){
                System.out.println("文件名："+fileName);
                System.out.println("作业名："+pushWork.getWorkName());
            }
        }

    }
}
