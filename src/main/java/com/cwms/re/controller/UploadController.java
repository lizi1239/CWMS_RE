package com.cwms.re.controller;

import com.cwms.re.pojo.CommitWork;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.UploadService;
import com.cwms.re.utils.file.FileUtils;
import com.cwms.re.utils.token.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
@ResponseBody
@RequestMapping("work")
public class UploadController {
    @Resource
    UploadService uploadService;
    @PostMapping("/commit")
    public BasicMsg uploading(MultipartFile file, String workId, String classId,HttpServletRequest httpServletRequest) throws IOException {
        String account = Jwt.getAudience(httpServletRequest.getHeader("token"));
        /** 保存文件*/
        String path = FileUtils.outFile(file, "commit_work");
        CommitWork commitWork = new CommitWork();
        commitWork.setWorkId(workId);
        commitWork.setClassId(classId);
        commitWork.setPath(path);
        return uploadService.uploading(account,commitWork);

    }
}
