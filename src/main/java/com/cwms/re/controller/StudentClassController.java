package com.cwms.re.controller;

import com.cwms.re.config.Global;

import com.cwms.re.pojo.StuClass;
import com.cwms.re.pojo.User;
import com.cwms.re.result.BasicMsg;
import com.cwms.re.service.StudentClassService;
import com.cwms.re.utils.token.Jwt;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import java.util.List;


@Controller
@ResponseBody //返回json
@RequestMapping("student")
public class StudentClassController {
    @Resource
    StudentClassService studentClassService;



    @GetMapping("/getclass")
    public List<StuClass> getclass(HttpServletRequest request){
        String account = Jwt.getAudience(request.getHeader("token"));


        return studentClassService.getclass(account);
    }




    @GetMapping("/in_class")
    public BasicMsg addClass(@RequestParam("class_id") String classId , HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("token");
        String account = Jwt.getAudience(token);
        if(Global.printDebugMsg){
            System.out.println("student addClass");
            System.out.println("classID"+classId);
            System.out.println(account);
        }

        return studentClassService.insert(account,classId);
    }

    @GetMapping("/out_class")
    public BasicMsg outClass(@RequestParam("class_id") String classId,HttpServletRequest httpServletRequest){
        String token = httpServletRequest.getHeader("token");
        String account = Jwt.getAudience(token);

        return  studentClassService.delete(account,classId);

    }

}
