package com.cwms.re.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwms.re.pojo.CommitWork;

public interface UploadMapper extends BaseMapper<CommitWork> {
}
