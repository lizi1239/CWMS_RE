package com.cwms.re.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwms.re.pojo.StuClass;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


public interface StudentClassMapper extends BaseMapper<StuClass> {
}
