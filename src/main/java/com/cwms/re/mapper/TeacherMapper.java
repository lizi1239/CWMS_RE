package com.cwms.re.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.cwms.re.pojo.Teacher;

import org.apache.ibatis.annotations.Select;

/**
 * 用途：对student表的crud
 *
 *
 * 作者：PSL
 * 日期：2021.05.13
 * 版本：0.0.1
 */
public interface TeacherMapper extends BaseMapper<Teacher> {
    @Select("select * from teacher where account=#{account}")
    public Teacher getByAccount(String account);

}
