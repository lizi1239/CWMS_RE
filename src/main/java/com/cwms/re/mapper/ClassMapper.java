package com.cwms.re.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwms.re.pojo.MyClass;

public interface ClassMapper extends BaseMapper<MyClass> {
}
