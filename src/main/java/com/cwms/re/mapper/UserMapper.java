package com.cwms.re.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwms.re.pojo.User;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * 用途：对user表的crud
 *
 *
 * 作者：PSL
 * 日期：2021.05.26
 * 版本：0.0.1
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {
    @Select("select * from user where account=#{account}")
//    @Select("select * from user where account='201842010109'")

    public User getByAccount(String account);
}
