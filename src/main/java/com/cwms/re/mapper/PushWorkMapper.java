package com.cwms.re.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.cwms.re.pojo.PushWork;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PushWorkMapper extends BaseMapper<PushWork> {
}
