package com.cwms.re.utils.hmac;


import com.cwms.re.controller.UserController;
import com.cwms.re.mapper.UserMapper;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.crypto.Mac;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.annotation.adapters.HexBinaryAdapter;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

@RunWith(SpringRunner.class)
@SpringBootTest
class TestHmac {

    @Autowired
    UserController userController;
    @Autowired
    MyHmac hmac;


   @Test
    public void testInitHmacKey() throws NoSuchAlgorithmException, InvalidKeyException {

//       System.out.println("生成MD5Key------------------------------------------------");
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacMD5Key()));
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacMD5Key()));
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacMD5Key()));
//
//       System.out.println("生成SHA256Key------------------------------------------------");
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacSHA256Key()));
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacSHA256Key()));
//       System.out.println(new HexBinaryAdapter().marshal(MyHmac.initHmacSHA256Key()));
//

       byte[] Key =  MyHmac.initHmacSHA256Key();
//       bin->hex
       String HKey = new HexBinaryAdapter().marshal(Key);
       System.out.println(HKey);

//       hex->hex
       byte[] BKey = new HexBinaryAdapter().unmarshal(HKey);
       System.out.println(new HexBinaryAdapter().marshal(BKey));
       // 还原密钥
       SecretKey secretKey = new SecretKeySpec(BKey, "HmacSHA256");
       System.out.println(secretKey);
       // 实例化Mac
       Mac mac = Mac.getInstance(secretKey.getAlgorithm());
       //初始化mac
       mac.init(secretKey);
       //执行消息摘要
       byte[] digest = mac.doFinal();
       System.out.println(new HexBinaryAdapter().marshal(digest));
//
//       byte[] MKey = new HexBinaryAdapter().unmarshal("jsdkjfkajwo我");
       System.out.println(new HexBinaryAdapter().marshal(digest));
       for(int i =  0;i<digest.length;i++){
           System.out.println(digest[i]&0xff);
       }
   }


    @Test
    public void testBase() throws Exception {
       String passWrod = "010109";
        System.out.println("原始密码："+ passWrod);
       String HS = Str2Hex2Bin.str2HexStr(passWrod);
        System.out.println("Hex原始密码："+HS);
        byte[] bin = Str2Hex2Bin.hexStr2bin(HS);
        System.out.println("Bin原始密码："+bin);
        byte[] salt =MyHmac.initHmacSHA256Key();
        System.out.println("salt："+salt);
        String saltHex = Str2Hex2Bin.bin2hexStr(salt);
        System.out.println("salt HexStr："+saltHex);
        System.out.println("salt HexStr Length："+saltHex.length());
        String sqlPassWord = MyHmac.encodeHmacSHA256(bin, salt);
        System.out.println("第1次加密的sql密码："+sqlPassWord);
        System.out.println("sql密码 Length："+sqlPassWord.length());
        byte[] sqlSaltBin = Str2Hex2Bin.hexStr2bin(saltHex);
        sqlPassWord = MyHmac.encodeHmacSHA256(bin, sqlSaltBin);
        System.out.println("第2次加密的sql密码："+sqlPassWord);


    }



}
